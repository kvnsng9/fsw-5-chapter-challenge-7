const { User } = require('../models' )
const passport = require('../lib/passport' )
module.exports = {
register : (req, res, next) => {
console.log(req.body);
 User.register (req.body)
 .then(() => {
 res.redirect ('/login' )
 })
 .catch(err => next(err))
},
index : (req, res, next) => {
    const user = req.user;
    console.log(user);
  res.render('home/beda', {user})
    },
    logout : (req, res, next) => {
        req.logout();
      res.redirect('/')
        },
login: passport.authenticate('local', {
    successRedirect: '/users',
    failureRedirect: '/login',
    failureFlash: true // Untuk mengaktifkan express flash
   })
}
