var express = require('express');
var router = express.Router();
var auth = require('../controler/authcontroler')
const restrict = require('../middleware/restrict')
/* GET users listing. */
router.get('/', restrict, auth.index)
router.post('/register' , auth.register );
router.post('/login', auth.login)
router.post('/logout', auth.logout)
module.exports = router;
