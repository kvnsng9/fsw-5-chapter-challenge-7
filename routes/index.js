var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('home/index');
});

router.get('/login', function(req, res, next) {
  res.render('home/login');
});

router.get('/signup', function(req, res, next) {
  res.render('home/signup');
});

module.exports = router;
